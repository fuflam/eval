package fr.umfds;

import com.github.lalyos.jfiglet.FigletFont;

import java.io.IOException;
import java.util.ArrayList;

public class Jeu {
    private Dico dico=new Dico();
    private String motCourant;
    private ArrayList<String> anciensMots=new ArrayList<>();

    public Jeu(Dico dico) throws IOException {
        this.dico=dico;
    }
    public Jeu() throws IOException {
    }

    public static void main( String[] args ) throws IOException {
        String asciiArt1 = FigletFont.convertOneLine("AGL");
        System.out.println(asciiArt1);
    }

    public String choixMot(int taille){
        String res="";
        while(res.equals("")) {
            String mot = dico.randomWord(taille);
            if (! anciensMots.contains(mot) && !mot.contains("-")){
                res=mot;
            }else{
                System.out.println("Mot non retenu : "+ mot);
            }
        }
        this.motCourant=res;
        anciensMots.add(res);
        return res;
    }

    public void choixManuelMot(String mot){
        motCourant=mot;
        anciensMots.add(mot);
    }


    public String analyse(String proposition){
        if (proposition.length()==motCourant.length()){
            char[] result=new char[motCourant.length()];
            for (int i=0;i<motCourant.length();i++){
                if (motCourant.charAt(i)==proposition.charAt(i)){
                    result[i]=motCourant.charAt(i);
                }else{
                    result[i]='_';
                }
            }
            return new String(result);
        }else{
            return "mot de longueur incorrecte";
        }
    }

    public void run(){
Scanner sc=new Scanner(System.in);
String proposition="";
boolean exit=false;
while (!exit&&!proposition.equals(motCourant)){
proposition=sc.nextLine();
if (!proposition.equals("EXIT")) {
String analyse = analyse(proposition);
System.out.println(analyse);
}else{
exit=true;
}
}
if (exit){
System.out.println("Le mot était "+motCourant);
}else {
System.out.println("Bravo !");
}
}

}

